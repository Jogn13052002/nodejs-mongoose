const db = require("../models/index");
const Tutorial = db.tutorials;
console.log("1");
// Create and Save a new Tutorial
exports.create = (req, res) => {
  // Validate request
  console.log("tutorial");
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  // Create a Tutorial
  const tutorial = new Tutorial({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false,
  });

  // Save Tutorial in the database
  tutorial
    .save(tutorial)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Tut",
      });
    });
};
exports.findAll = (req, res) => {
  const title = req.query.title;
  Tutorial.find()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving tutor",
      });
    });
};
exports.findOne = (req, res) => {
  const title = req.query.title;
  let id = req.params.id;
  Tutorial.findOne({ _id: id })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving tutor",
      });
    });
};
// module.exports.create = create;
