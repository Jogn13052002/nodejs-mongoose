const tutorials = require("../controller/tutorial");
module.exports = (app) => {
  var router = require("express").Router();
  // Create a new Tutorial
  router.post("/create", tutorials.create);
  router.get("/create", function (req, res) {
    console.log("tutorial");
    res.send("sad");
  });
  // Retrieve all Tutorials
  router.get("/getAll", tutorials.findAll);
  router.get("/:id", tutorials.findOne);
  // // Retrieve all published Tutorials
  // router.get("/published", tutorials.findAllPublished);
  // // Retrieve a single Tutorial with id
  // router.get("/:id", tutorials.findOne);
  // // Update a Tutorial with id
  // router.put("/:id", tutorials.update);
  // // Delete a Tutorial with id
  // router.delete("/:id", tutorials.delete);
  // // Delete all Tutorials
  // router.delete("/", tutorials.deleteAll);
  // app.use("/api/tutorials", router);
  return router;
};
